import mysql from "mysql2"
import 'dotenv/config'
import {getSignedUrl} from "@aws-sdk/s3-request-presigner"
import {s3cl} from "./index.js"
import axios from "axios"
const region = 'us-east-2'
const bucket = "bayubebek"
// app.set('view engine', ejs)
import {
    SecretsManagerClient,
    GetSecretValueCommand,
  } from "@aws-sdk/client-secrets-manager";
import { PutObjectCommand, GetObjectCommand, DeleteObjectCommand } from "@aws-sdk/client-s3"
const secret_name = "proddb";
// app.get('/', (req, res) =>{
//     res.render(view, locals)
// })

// AWS.config.update({
//     accessKeyId: "AKIA4NGK4APLTSHRGAHH",
//     secretAccessKey: "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",
//     region: region,
//   });
  const client = new SecretsManagerClient({
    region: region,
  });
  
  let response;
  
  try {
    response = await client.send(
      new GetSecretValueCommand({
        SecretId: secret_name,
        VersionStage: "AWSCURRENT", // VersionStage defaults to AWSCURRENT if unspecified
      })
    );
  } catch (error) {
    // For a list of exceptions thrown, see
    // https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
    throw error;
  }
  
  const secret = JSON.parse(response.SecretString);
  
  // Your code goes here

const pool = mysql.createPool({
        host: "127.0.0.1",//secret.host,
        user: secret.username,
        password: secret.password,
        database: 'perpustakaan',
        timezone : "+00:00"
    }).promise()

export async function getpinjam(){
    const [rows] = await pool.query("SELECT * FROM perpus")
    return rows
}

export async function bukupinjam(id){
    const [rows] = await pool.query(`SELECT * FROM perpus WHERE idbuku = ?`, [id])
    return rows
}

export async function pinjambaru(id,nama,tglp,tglk,imagefile,objecturl){
    const result= await pool.query(`INSERT INTO perpus (idbuku, nama, tanggal_pinjam, tanggal_kembali, imagefile, objecturl) VALUES (?, ?, ?, ?, ?, ?)`, [id, nama, tglp, tglk, imagefile, objecturl])
    return result
}

export async function hapuspinjam(id){
    const [[gambar]] = await pool.query(`SELECT imagefile FROM perpus WHERE idbuku = ?`, [id])
    if (gambar.imagefile != "" || " " || null){
        const input = {Bucket: bucket, Key: `${gambar.imagefile}`}
        await new DeleteObjectCommand(input, function(err,data){
            if(err){
                console.log(err)
            }else{
                console.log("deleted")
        }})
      }else{
        console.log("nah, fam. not today")
    }
    const result = await pool.query(`DELETE FROM perpus WHERE idbuku = ?`, [id])
    return result
}

export async function updatepinjam(id, tglk){
    const result= await pool.query(`UPDATE perpus SET tanggal_kembali = ? WHERE perpus.idbuku = ?`, [tglk, id])
    return result
}

export async function getsignedurl(filename){
  const url = await getSignedUrl(s3cl, new PutObjectCommand({
    Bucket: bucket,
    Key: `img/${filename}`
  }),{expiresIn: 10})
  return url
}

export async function uploadfile(url, file){
  const upload = await axios.put(url,file)
  return upload
}

// export async function getfile(url){
//   const upload = await axios.get(url)
//   return upload
// }

export async function getimageurl(filename){
  const url = await getSignedUrl(s3cl, new GetObjectCommand({
    Bucket: bucket,
    Key: `img/${filename}`
  }),{expiresIn: 15})
  return url
}

export async function getbooks(){
  const search = "https://www.googleapis.com/books/v1/volumes?key=AIzaSyATQztvosP8P57iKrOc4CzgfDBp5kCUm2M&q=\"\""
  const results = await axios.get(search)
  return results.data
}

export async function searchbook(title){
  const search = "https://www.googleapis.com/books/v1/volumes?key=AIzaSyATQztvosP8P57iKrOc4CzgfDBp5kCUm2M&q=" + title
  const results = await axios.get(search)
  return results.data
}

export async function getbookbyid(id){
  const search = `https://www.googleapis.com/books/v1/volumes/${id}?key=AIzaSyATQztvosP8P57iKrOc4CzgfDBp5kCUm2M&id=`
  const results = await axios.get(search)
  return results.data
}
