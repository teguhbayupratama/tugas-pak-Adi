import express from "express"
import request from "request"
import cors from "cors"
import path from "path"
import 'dotenv/config'
// import ejs from "ejs"
import multer from "multer"
import {S3Client} from "@aws-sdk/client-s3"
import { getpinjam, bukupinjam, pinjambaru, hapuspinjam, updatepinjam, getsignedurl, uploadfile, getimageurl, getbooks, searchbook, getbookbyid } from './server.js'
import { fileURLToPath } from 'url';
import { dirname } from 'path';
const region = "us-east-2"


const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const s3cl = new S3Client({
    region: region,
})

const upload = multer({ storage: multer.memoryStorage() });

const app = express()

app.set("view engine", "ejs");
app.use(cors())
app.use(express.json())
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'views')));

app.get("/", function (req, res) {
    res.render("index");
});

app.get("/peminjaman", function (req, res) {
    res.render("peminjaman");
});

app.get("/api/v1/buku", async (req,res)=>{
    const books = await getbooks()
    res.send(books)
}) 

app.get("/api/v1/buku/search/:title", async (req,res)=>{
    const books = await searchbook(req.params.title)
    res.send(books)
}) 

app.get("/api/v1/buku/id/:id", async (req,res)=>{
    const books = await getbookbyid(req.params.id)
    res.send(books)
}) 

app.get("/api/v1/peminjaman/:id", async (req,res)=>{
    const id = req.params.id
    const pinjam = await bukupinjam(id)
    res.send(pinjam)
}) 

app.get("/api/v1/peminjaman", async (req,res)=>{
    const pinjam = await getpinjam()
    res.send(pinjam)
}) 

app.get("/api/v1/peminjaman/gambar/:filename", async (req,res)=>{
    const filename = decodeURI(req.params.filename)
    const gambar = await getimageurl(filename)
    request(gambar).pipe(res);
}) 

app.post("/api/v1/peminjaman/gambar", upload.single("image"), async (req,res)=>{
    const url = await getsignedurl(req.file.originalname)
    // console.log(url)
    await uploadfile(url, req.file.buffer)
    res.redirect("/")
})

app.post("/api/v1/peminjaman/baru", async (req,res)=>{
    const {id, nama, tglp, tglk, imagefile} = req.body
    const objecturl = encodeURIComponent(imagefile.trim()) 
    const pinjam = await pinjambaru(id, nama, tglp, tglk, imagefile, objecturl)
    res.send(pinjam)
})

app.put("/api/v1/peminjaman/update", async (req,res)=>{
    const {id, tglk} = req.body
    const pinjam = await updatepinjam(id, tglk)
    res.send(pinjam)
})

app.delete("/api/v1/peminjaman/hapus/:id", async (req,res)=>{
    const id = req.params.id
    // fs.unlink(`views/uploads/${foto.imagefile}`)
    const hapus = await hapuspinjam(id)
    res.send(hapus)
})
// app.post("/posts", (req,res) => {
//     console.log(req.body)})
app.use("*", (req, res) => 
    res.status(404).json({ error: "ga ada" }))

app.listen(80, () =>{
    console.log("mendengarkan di port 80")
})

export default app