const search = "http://127.0.0.1:3000/api/v1/buku/"
const api = 'http://127.0.0.1:3000/api/v1/peminjaman/'

const content = document.getElementById("content")
const query = document.getElementById("query")
const form = document.getElementById("form")

let date_ob = new Date()
let tgl
let date = ("0" + date_ob.getDate()).slice(-2);
let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
let year = date_ob.getFullYear();
let hours = date_ob.getHours();
let minutes = date_ob.getMinutes();

timenow = time()
listbuku(search);

// async function cekbuku(id){
//   lah = await bukusatu(id).then(function(books){
//     leh = isEmpty(books)
//     return leh
//   })
// }

function listbuku(url){
    fetch(url).then(res => res.json()).then(function(data){
        // console.log(data.items);
        data.items.forEach(buku =>{
            bukusatu(buku.id).then(function(books){
                        leh = isEmpty(books)
                        if(leh){
                          hasil = true
                          // return hasil
                        }else{
                          hasil =false
                          // return hasil
                        }
                        gambar = buku.volumeInfo.imageLinks.thumbnail
                        const div_card = document.createElement('div');
                        div_card.setAttribute('class', 'card');
                        div_card.setAttribute('style', 'style="width: 10rem;"')

                        // const div_row = document.createElement('div');
                        // div_row.setAttribute('class', 'items');
                        
                        const div_column = document.createElement('div');
                        div_column.setAttribute('class', 'item-detail items mx-2 my-4');
                        
                        const div_cardbody = document.createElement('div');
                        div_cardbody.setAttribute('class', 'card-body');

                        const image = document.createElement('img');
                        image.setAttribute('class', 'card-img-top object-fit-cover');
                        image.setAttribute('id', 'image');
                        
                        const title = document.createElement('h5');
                        title.setAttribute('id', 'title');
                        title.setAttribute('class', 'card-titleq');
                        
                        const author = document.createElement('p');
                        author.setAttribute('id', 'author');
                        author.setAttribute('class', 'card-text pb-2');
                        
                        const tooltip = document.createElement('span');
                        tooltip.setAttribute('data-toggle', "tooltip")
                        tooltip.setAttribute('data-bs-placement', 'bottom')
                        tooltip.setAttribute('title', `Sedang dipinjam`)
                        const button = document.createElement('a');
                        button.setAttribute('id', 'btnpinjam');
                        button.setAttribute('href', '3');
                        
                        button.setAttribute('data-bs-toggle', 'modal')
                        button.setAttribute('data-bs-target', `#staticBackdrop${buku.id}`)
                        const center = document.createElement('center');
                        
                        button.innerHTML = `Pinjam`
                        title.innerHTML = `${buku.volumeInfo.title.substring(-1, 20)}...`;
                        author.innerHTML = `${buku.volumeInfo.authors}`;
                        image.src = `${gambar}`;

                        const dialog = document.createElement('div');
                        dialog.innerHTML = `<div class="modal fade" id="staticBackdrop${buku.id}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h1 class="modal-title fs-5" id="staticBackdropLabel">Pinjam Buku</h1>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                              <div class="mb-3">
                                <label for="namapeminjam${buku.id}" class="form-label">Nama Peminjam</label>
                                <input class="form-control" id="namapeminjam${buku.id}">
                                <div class="form-text">Nama pendek saja.</div>
                              </div>
                              <div class="mb-3">
                                <label for="tanggalpeminjaman${buku.id}" class="form-label">Tanggal Peminjaman</label>
                                <input class="form-control" id="tanggalpeminjaman${buku.id}" value="${timenow}" disabled>
                              </div>
                              <div class="mb-3">
                                <label for="tanggalkembali${buku.id}" class="form-label">Tanggal Kembali</label>
                                <input class="form-control" id="tanggalkembali${buku.id}">
                                <div class="form-text">YYYY-MM-DD</div>
                              </div>
                              <div class="mb-3">
                               <form method="POST" action="/api/v1/peminjaman/gambar" enctype="multipart/form-data" id="form${buku.id}">
                                    <label for="file${buku.id}" class="form-label">Upload Foto Wajah</label>
                                    <input class="form-control" type="file" accept="image/*" name="image" id="file${buku.id}">
                               </form>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-primary" data-bs-dismiss="modal" onclick="submit('${buku.id}')">Pinjam</button>
                              </div>
                            </div>
                            
                          </div>
                        </div>
                      </div>`;
                
                        center.appendChild(title);
                        center.appendChild(author);
                        if (hasil){
                          button.setAttribute('class', 'btn btn-primary')
                          center.appendChild(button);
                        }else{
                          button.setAttribute('class', 'btn btn-secondary disabled')
                          button.setAttribute('aria-disabled', 'true')
                          tooltip.appendChild(button)
                          center.appendChild(tooltip);
                        }
                        div_cardbody.appendChild(center);
                        div_card.appendChild(image);
                        div_card.appendChild(div_cardbody);
                        div_column.appendChild(div_card);
                        div_column.appendChild(dialog);
                        // div_row.appendChild(div_column);
                        


                        content.appendChild(div_column);
                        // document.getElementById('content').insertAdjacentHTML('beforeend', html_to_insert);
                        // content.appendChild(dialogtext);
                      })

                  
            // waw = cekbuku(buku.id)
              
            
            
        });
    });
}

function bukusatu(id){
    // return fetch(api+id).then(res => res.json()).then(function(result){
    //   return result
    // })
    return fetch(api+id).then(res => res.json()).then(data => {
      return data
      })
}

// bukusatu().then(function(result) {
//   console.log(result);
// });

form.addEventListener("submit", (e) =>{
    e.preventDefault();
    content.innerHTML= ''

    const searchitem = query.value

    if(searchitem){
        listbuku(search+`search/${searchitem}`);
        search.value= "";
    }

});

function isEmpty(obj) {
  if(Object.keys(obj).length === 0){
    return true;
  }
  return false
}

function submit(id){
    const nama = document.getElementById(`namapeminjam${id}`).value
    const tglp = document.getElementById(`tanggalpeminjaman${id}`).value
    const tglk = document.getElementById(`tanggalkembali${id}`).value
    const gambar = document.getElementById(`file${id}`).files[0].name
    const tombol = document.getElementById(`form${id}`)
    let formData = new FormData(); 
    formData.append("image", gambar);
    fetch(api + "baru",{
        method: "POST",
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
      },
        body: JSON.stringify({"id": id, "nama": nama, "tglp": tglp, "tglk": tglk, "imagefile": gambar})
    })
    .then(res => res.json())
    .then(res =>{
        tombol.submit()
    })
}

function time(){
    tgl = year + "-" + month + "-" + date
    return tgl
}


// function waw(id, nama, tglp, tglk){
//     fetch(api + "baru",{
//     method: "POST",
    // headers: {
    //     'Accept': 'application/json, text/plain, */*',
    //     'Content-Type': 'application/json'
    // },
//     body: JSON.stringify({"idbuku": id, "nama": nama, "tglp": tglp, "tglk": tglk})
// })
// .then(res => res.json())
// }

// waw('IVJ8BgAAQBAJ', 'bayu', '2023-09-02', '2023-09-08')