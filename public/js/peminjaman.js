const search = "http://127.0.0.1:3000/api/v1/buku/id/"
const api = 'http://127.0.0.1:3000/api/v1/peminjaman/'

const main = document.getElementById("content")

const content = document.getElementById("content")
bukupinjaman(api)

function bukusatu(id){
  return fetch(search + id).then(res => res.json()).then(function(data){
    datapeminjaman = data.volumeInfo
    return datapeminjaman
  })
}

function bukupinjaman(apir){
  const buk = document.getElementById('content')
    fetch(apir).then(res => res.json()).then(function(data){
        data.forEach(buku =>{
            const panel = document.createElement('div')
            bukusatu(buku.idbuku).then(function(datapinjam){
                  const gambar = datapinjam.imageLinks
                  const datekembali = buku.tanggal_kembali.substring(0, 10)
                  const datepinjam = buku.tanggal_pinjam.substring(0, 10)
                  const objecturl = buku.objecturl
                  panel.innerHTML=`<div>
                  <div class="card cardwaw" id="C${buku.idbuku}">
                  <div class="card-body">
                      <img src="${gambar.thumbnail}" class="position-absolute" style="width: 14%;height: 75%;">
                      <div class="dropdown float-end me-3 mt-2">
                          <a class="" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-three-dots" viewBox="0 0 16 16">
                                  <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"/>
                              </svg>
                          </a>
                          <ul class="dropdown-menu">
                            <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#D${buku.idbuku}hapus" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                            <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
                          </svg>  Kembalikan Buku</a></li>
                            <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#D${buku.idbuku}edit" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                            <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                          </svg>  Perpanjang peminjaman</a></li>
                          <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#D${buku.idbuku}foto" href="#">
                            <i class="fa-regular fa-face-smile fa-sm" style="color: #ffffff;"></i>
                            Foto peminjam</li>
                          </ul>
                      </div>
                      <div class="textcontainer px-4">
                        <h5 class="card-title ms-5 mt-1 ps-5">${datapinjam.title}</h5>
                      <p class="card-text ms-5 mt-2 ps-5">
                        Peminjam: ${buku.nama}
                      </p>
                      <p class="card-text ms-5 mt-1 ps-5">
                        Tanggal Pengembalian: ${datekembali}
                      </p>
                      </div>
                      <p class="card-text"><small class="text-body-secondary d-flex justify-content-end">${datepinjam}</small></p>
                  </div>
              </div>
              <div class="modal fade" id="D${buku.idbuku}hapus" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h1 class="modal-title fs-5" id="staticBackdropLabel">Pengembalian buku</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                        Apakah anda yakin ingin mengembalikan buku ini?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-secondary" onclick="del('${buku.idbuku}')">Kembalikan</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal fade" id="D${buku.idbuku}foto" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h1 class="modal-title fs-5" id="staticBackdropLabel">Foto Peminjam</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                        <center><img src="${api}gambar/${objecturl}" alt="foto tersangka peminjam" style="max-width: 95%;"></center>
                      </div>
                </div>
                <div class="modal fade" id="D${buku.idbuku}edit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h1 class="modal-title fs-5" id="staticBackdropLabel">Edit data</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Nama Peminjam</label>
                          <input class="form-control" id="namapeminjam${buku.idbuku}" value="${buku.nama}" disabled>
                        </div>
    
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Tanggal Peminjaman</label>
                          <input class="form-control" id="tanggalpeminjaman${buku.idbuku}" value="${datepinjam}" disabled>
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Tanggal Kembali</label>
                          <input class="form-control" id="tanggalkembali${buku.idbuku}">
                          <div class="form-text">YYYY-MM-DD</div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="save('${buku.idbuku}')">Update</button>
                      </div>
                </div>
              </div>
              </div>
              </div>`
            main.appendChild(panel)
            })
            // const gambar = bukusatu(buku.idbuku).imageLinks
          })
        })
      }

function del(id){
  fetch(api + "hapus/" + id,{
      method: "DELETE",
  })
  .then(res => res.json())
  .then(res =>{
      location.reload()
  })
}

function save(id){
        const tglk = document.getElementById("tanggalkembali"+id).value
        fetch(api + "update",{
            method: "PUT",
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({"id": id, "tglk": tglk})
        })
        .then(res => res.json())
        .then(res =>{
            location.reload()
        })
}

